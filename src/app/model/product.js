const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema(
  {
    name: {
        type: String,
        required: true
      },
    amount: {
        type: Number,
        required: false
      },
    price: {
        type: Number,
        required: true
      },
    installments: {
        type: String,
        required: false
      },
    paid: {
        type: String,
        required: false
      },
    beginning: {
        type: Date,
        required: false
      }
  },
  {
    timestamps: true
  }
);


module.exports = mongoose.model("Product", ProductSchema);