const mongoose = require("mongoose");

const ListProductSchema = new mongoose.Schema(
    {
      name: {
          type: String,
          required: true
        },
    },
    {
      timestamps: true
    }
  );

module.exports = mongoose.model("ListProduct", ListProductSchema);
