const mongoose = require("mongoose");

const CountrySchema = new mongoose.Schema(
  {
    name: {
        type: String,
        required: true
      },
    state: [
       { 
        name: String
       }
    ]
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Country", CountrySchema);