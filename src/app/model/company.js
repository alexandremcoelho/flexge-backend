const mongoose = require("mongoose");

const CompanySchema = new mongoose.Schema(
  {
    name: {
        type: String,
        required: true
      },
    document: {
        type: Number,
        required: true
      },
    social_reason: {
        type: String,
        required: true
      },
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Company", CompanySchema);