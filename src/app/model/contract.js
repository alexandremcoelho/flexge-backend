const mongoose = require("mongoose");

const ContractSchema = new mongoose.Schema(
  {
    company : { type: mongoose.Schema.Types.ObjectId, ref: 'Company' },

    country: { type: mongoose.Schema.Types.ObjectId, ref: 'Country' },

    document: {
      type: String,
      required: false
    },

    city: {
        type: String,
        required: false
      },
    social_reason: {
        type: String,
        required: true
      },
    address: {
        type: String,
        required: true
      },
    district: {
        type: String,
        required: true
      },
    number: {
        type: Number,
        required: true
      },
    zip_code: {
        type: Number,
        required: true
      },
    email: {
        type: String,
        required: true
      },
    phone: {
        type: Number,
        required: false
      },
    start: {
        type: String,
        required: true
      },
    end: {
        type: String,
        required: false
      },
    due_day: {
        type: String,
        required: true
      },
    products: [
      {
          name: {
            type: String,
            required: true
          },
          amount: {
            type: Number,
            required: false
          },
          price: {
            type: Number,
            required: true
          },
          installments: {
            type: String,
            required: false
          },
          paid: {
            type: String,
            required: false
          },
          beginning: {
            type: String,
            required: false
          },
      }
    ]
    
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("Contract", ContractSchema);