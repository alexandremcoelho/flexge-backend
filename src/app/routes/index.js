const express = require('express');
const companyController = require('../controllers/companyController');
const countryController = require('../controllers/countryController');
const contractController = require('../controllers/contractController')
const UserController = require('../controllers/userController')
const ProductController = require("../controllers/productController")

const router = express.Router();

router
    .route('/Company')
    .get(companyController.list)
    .post(companyController.create);

router
    .route('/Country')
    .get(countryController.list)
    .post(countryController.create);

router
    .route('/Contract')
    .get(contractController.list)
    .post(contractController.create);

router
    .route('/Contract/:id')
    // .patch(contractController.update)
    .delete(contractController.delete);

router
    .route('/Login')
    .post(UserController.login);

router
    .route('/Product')
    .get(ProductController.listAllProducts)
    .post(ProductController.create);

module.exports = router;