const Router = require("express").Router;
const User = require("../model/user");

class UserController {

    login = async (req, res) => {

        const { name, password } = req.body;

        if(!name || !password) return res.status(400).json({message: 'Requisição invalida!'})

        try {
            const userData = await User.find({ name: name })

            if(!userData) return res.status(404).json({message: 'Usuario não encontrado!'})
            if(userData.length > 1) return res.status(500).json({message: 'Usuario duplicado!'})
            if(userData[0].password !== password ) return res.status(400).json({message: 'Senha incorreta!'})

            return res.status(200).json({message: 'Logado'})

        } catch (error) {
            return res.status(500).json({Error: error})
        }
      };
    
}

module.exports = new UserController()