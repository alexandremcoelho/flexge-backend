const Router = require("express").Router;
const Product = require("../model/product");
const ListProduct = require("../model/products");

class ProductController {
    listAllProducts =   async (req, res) => {
        try {
            const data = await ListProduct.find({});
            return res.json(data);
        } catch (error) {
            return res.status(500).json({Error: error})
        }
      }

    create = async (req, res) => {

        const { name, amount, price, installments, paid, beginning } = req.body;

        if(!name || !price ) return res.status(400).json({message: 'Requisição invalida!'})

        const produc = {
                name,
        }

        try {
            await Product.create(produc);
            return res.status(201).json({message: 'Criada'})
        } catch (error) {
            return res.status(500).json({Error: error})
        }
      };
    
}

module.exports = new ProductController()