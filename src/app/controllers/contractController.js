const Router = require("express").Router;
const Contract = require("../model/contract");

class ContractController {
    list =   async (req, res) => {
        try {

            const { document, social_reason, company} = req.query; 
            let data = await Contract.find({}).populate('company');

            if(typeof document !== 'undefined' && document !== 'undefined'){
                data = data.filter(item => item.document.toLowerCase().includes(document.toLowerCase()))
            }
            if(typeof social_reason !== 'undefined'  && social_reason !== 'undefined'){
                data = data.filter(item => item.social_reason.toLowerCase().includes(social_reason.toLowerCase()))
            }
            if(typeof company !== 'undefined'  && company !== 'undefined'){
                data = data.filter(item => item.company.name.toLowerCase.includes(company.toLowerCase))
            }

            return res.json(data);
        } catch (error) {
            return res.status(500).json({Error: error})
        }
      }

    create = async (req, res) => {

        const { company, country, document, state, city, social_reason, address, district, number, zip_code, email, phone,
              start, end, due_day, products} = req.body;

        if(!company || !country || !document || !state || !social_reason || !address || !district || !number || !zip_code || !email || !start || !due_day)
        {
         return res.status(400).json({message: 'Requisição invalida!'})
        }

        const contract = { 
            company,
            country,
            document,
            state,
            city,
            social_reason, 
            address, 
            district, 
            number, 
            zip_code, 
            email, 
            phone,
            start, 
            end, 
            due_day,
            products
        }


        try {
            await Contract.create(contract);
            return res.status(201).json({message: 'Criada'})
        } catch (error) {
            return res.status(500).json({Error: error})
        }

      };
    
      delete =  async (req, res) => {
        try {
            await Contract.deleteOne({ _id: req.params.id });
            res.status(204)
        } catch (error) {
            return res.status(500).json({Error: error})
        }
      }

    //   update =  async (req, res) => {
    //     try {
    //         const id = req.params.id


    //         return res.json(data);
    //     } catch (error) {
    //         return res.status(500).json({Error: error})
    //     }
    //   }
}
module.exports = new ContractController()
