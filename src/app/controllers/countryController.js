const Router = require("express").Router;
const Country = require("../model/country");

class CountryController {
    list =   async (req, res) => {
        try {
            const data = await Country.find({});
            return res.json(data);
        } catch (error) {
            return res.status(500).json({Error: error})
        }
      }

    create = async (req, res) => {

        const { name, state } = req.body;

        if(!name || !state) return res.status(400).json({message: 'Requisição invalida!'})

        const country = {
                name,
                state: state
        }

        try {
            await Country.create(country);
            return res.status(201).json({message: 'Criada'})
        } catch (error) {
            return res.status(500).json({Error: error})
        }
      };
    
}

module.exports = new CountryController()