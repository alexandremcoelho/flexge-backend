const Router = require("express").Router;
const Company = require("../model/Company");

class companyController {
    list =   async (req, res) => {
        try {
            const data = await Company.find({});
            return res.json(data);
        } catch (error) {
            return res.status(500).json({Error: error})
        }

      }

    create = async (req, res) => {

        const { name, social_reason, document, } = req.body;

        if(!name || !social_reason || !document) return res.status(400).json({message: 'Requisição invalida!'})

        const company = {
            name,
            document,
            social_reason
        }

        try {
            await Company.create(company);
            return res.status(201).json({message: 'Compania Criada'})
        } catch (error) {
            return res.status(500).json({Error: error})
        }
      };
    
}

module.exports = new companyController()